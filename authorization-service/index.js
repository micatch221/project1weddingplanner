const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore()


async function checkPassword(reqBody){
    const user = reqBody;
    const query = datastore.createQuery('employee').filter('email','=',user.email).filter('password','=',user.password)
    const [data,metaInfo] = await datastore.runQuery(query);
    if (data)
    {
        const name = {fname:data[0].fname, lname:data[0].lname}
        console.log(name)
    }else{
        console.log("No user found")
    }
    
}

const emp = {email:'tester1@wed.com',password:'password'}
checkPassword(emp)


app.get('/users/:email/verify',async (req,res)=>{
    try{
        const query = datastore.createQuery('employee').filter('email','=',req.params.email);
        const [data,metaInfo] = await datastore.runQuery(query);
        res.status(200).send(data[0].fname)
    }catch (error){
            res.status(404).send("No user found")
        }
})

app.patch('/users/login',async (req,res)=>{
    try {
        const user = req.body;
        const query = datastore.createQuery('employee').filter('email','=',user.email).filter('password','=',user.password)
        const [data,metaInfo] = await datastore.runQuery(query);
        const name = {fname:data[0].fname, lname:data[0].lname}
        res.send(name)
    } catch (error){
            res.status(404).send("User not found")
        }
})
 const PORT = process.env.PORT || 3002
app.listen(PORT, ()=>console.log('Application started'))