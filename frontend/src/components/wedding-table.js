
export default function WeddingTable(props){

    const weddings = props.weddings;
    return(<table>
        <thead><tr><th>ID</th><th>Date</th><th>Location</th><th>Name</th><th>Budget</th></tr></thead>
        <tbody>
            {weddings.map(w => <tr key={w.weddingId}>
                <td>{w.weddingId}</td>
                <td>{w.weddingDate}</td>
                <td>{w.weddingLocation}</td>
                <td>{w.weddingName}</td>
                <td>{w.weddingBudget}</td>
            </tr>
            )}
        </tbody>
    </table>)
}