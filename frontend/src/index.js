import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ViewerPage from './components/viewer-page';
import './index.css';
import SignInSide from './components/login-page.js';
import Dashboard from './components/dashboard.js';
import App from './components/convertBase64.js';

ReactDOM.render(
  <React.StrictMode>
  <Router>
    <Switch>
      <Route path="/view">
        <ViewerPage></ViewerPage>
      </Route>
      <Route path="/login">
        <SignInSide></SignInSide>
      </Route>
      <Route path="/dashboard">
        <Dashboard></Dashboard>
      </Route>
      <Route path="/file">
        <App></App>
      </Route>
      <Route path="/">
          <SignInSide></SignInSide>
      </Route>
      
   </Switch>
  </Router>
    
  </React.StrictMode>,
  document.getElementById('root')
);
