import { client } from "../connection";
import { Wedding, Expense } from "../entities";
import { MissingResourceError } from "../errors";
import { WeddingDAO } from "./wedding-dao";

export class WeddingDAODB implements WeddingDAO{
    
    
    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into wedding(wed_date,wed_location,wed_name,budget) values ($1,$2,$3,$4) returning wedding_id";
        const values = [wedding.weddingDate,wedding.weddingLocation,wedding.weddingName,wedding.weddingBudget];
        const result = await client.query(sql,values);
        wedding.weddingId = result.rows[0].wedding_id;
        return wedding;
    }
    async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "insert into expense(reason,amount,w_id) values ($1,$2,$3) returning expense_id";
        const values = [expense.expenseReason,expense.expenseAmount,expense.expWId];
        const result = await client.query(sql,values);
        expense.expenseId = result.rows[0].expense_id;
        return expense;
    }
    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.wedding_id,
                row.wed_date,
                row.wed_location,
                row.wed_name,
                row.budget);
                weddings.push(wedding);
        }
        return weddings;
    }
    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where wedding_id = $1';
        const values = [weddingId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${weddingId} does not exist`);
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(
            row.wedding_id,
            row.wed_date,
            row.wed_location,
            row.wed_name,
            row.budget);
        return wedding;
    }
    async getExpenseById(expenseId: number): Promise<Expense> {
        const sql:string = 'select * from expense where expense_id = $1';
        const values = [expenseId]
        const result = await client.query(sql,values)
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        const row = result.rows[0];
        const expense:Expense = new Expense(
            row.expense_id,
            row.reason,
            row.amount,
            row.w_id);
        return expense;
    }

    async getAllExpensesByWeddingId(weddingId: number): Promise<Expense[]> {
        const sql:string = 'select * from expense where w_id = $1';
        const values = [weddingId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`No expenses exist for wedding with id: ${weddingId}`);
        }
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
            row.expense_id,
            row.reason,
            row.amount,
            row.w_id);
            expenses.push(expense);
        }
        return expenses;
    }

    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = 'select * from expense';
        const result = await client.query(sql);
        if(result.rowCount === 0){
            throw new MissingResourceError(`No expenses found`);
        }
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
            row.expense_id,
            row.reason,
            row.amount,
            row.w_id);
            expenses.push(expense);
        }
        return expenses;
    }
    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set wed_date=$1, wed_location=$2 ,wed_name=$3, budget=$4 where wedding_id =$5';
        const values = [wedding.weddingDate,wedding.weddingLocation, wedding.weddingName, wedding.weddingBudget,wedding.weddingId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${wedding.weddingId} does not exist`);
        }
        return wedding;
    }

    async updateExpenseById(expense:Expense): Promise<Expense> {
        const sql:string = 'update expense set reason=$1, amount=$2, w_id=$3 where expense_id=$4 ';
        const values = [expense.expenseReason,expense.expenseAmount,expense.expWId,expense.expenseId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expense.expenseId} does not exist`);
        }
        return expense;
    }
    async deleteWeddingById(weddingId: number): Promise<boolean> {
        const sql:string = 'delete from wedding where wedding_id=$1';
        const values = [weddingId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${weddingId} does not exist`);
        }
        return true
    }
    async deleteExpenseById(expenseId: number): Promise<boolean> {
        const sql:string = 'delete from expense where expense_id=$1';
        const values = [expenseId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        return true
    }
}