
import { Wedding } from "../entities";
import { Expense } from "../entities";

export interface WeddingDAO{

    createWedding(wedding:Wedding):Promise<Wedding>;
    createExpense(expense:Expense):Promise<Expense>;

    getAllWeddings():Promise<Wedding[]>;
    getWeddingById(weddingId:number):Promise<Wedding>;
    getAllExpensesByWeddingId(weddingId:number):Promise<Expense[]>
    getAllExpenses():Promise<Expense[]>;
    getExpenseById(expenseId:number):Promise<Expense>;

    updateWedding(wedding:Wedding):Promise<Wedding>;
    updateExpenseById(expense:Expense):Promise<Expense>;

    deleteWeddingById(weddingId:number):Promise<boolean>;
    deleteExpenseById(expenseId:number):Promise<boolean>;
}