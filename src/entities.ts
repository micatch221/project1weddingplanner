
export class Wedding{
    constructor(
        public weddingId:number,
        public weddingDate:number,
        public weddingLocation:string,
        public weddingName:string,
        public weddingBudget:number
    ){}
}

export class Expense{
    constructor(
        public expenseId:number,
        public expenseReason:string,
        public expenseAmount:number,
        public expWId:number
    ){}
}