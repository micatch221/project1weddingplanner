import express from 'express';
import cors from 'cors'
import { WeddingServiceImpl } from './services/wedding-service-impl';
import WeddingService from './services/wedding-service';
import { Wedding, Expense } from './entities';
import { MissingResourceError } from './errors';


const app = express();
app.use(express.json());
app.use(cors())

const weddingService:WeddingService = new WeddingServiceImpl();

app.post("/weddings", async (req, res) =>{
    let wedding = req.body;
    wedding = await weddingService.addNewWedding(wedding);
    res.status(201);
    res.send(wedding)

});

app.post("/expenses", async (req, res) =>{
    let expense = req.body;
    expense = await weddingService.addNewExpense(expense);
    res.status(201);
    res.send(expense)
});

app.get("/weddings", async (req, res) =>{
    const weddings:Wedding[] = await weddingService.listAllWeddings();
    res.send(weddings);
});

// GET /wedding/:id

app.get("/weddings/:id", async (req,res) =>{
    try {
    const weddingId = Number(req.params.id);
    const wedding:Wedding = await weddingService.findWeddingById(weddingId);
    res.send(wedding);
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

// GET /weddings/:id/expenses
app.get("/weddings/:id/expenses", async (req,res) =>{
    try{
        const weddingId = Number(req.params.id);
        const expenses:Expense[] = await weddingService.listAllExpensesByWeddingId(weddingId);
        res.send(expenses)
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})

// DELETE /weddings/:id

app.delete("/weddings/:id", async (req,res)=>{
    try {
        const weddingId = Number(req.params.id);
        const isDeleted = await weddingService.removeWeddingById(weddingId);
        res.status(205);
        res.send(isDeleted);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});
// PUT /weddings
app.put("/weddings", async (req,res)=>{
    try {
    let wedding = req.body;
    wedding = await weddingService.modifyWedding(wedding);
    res.status(200);
    res.send(wedding) 
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})
// GET /expenses
app.get("/expenses", async (req,res)=>{
    try {
        const expenses:Expense[] = await weddingService.listAllExpenses();
        res.status(200);
        res.send(expenses)
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})
// GET /expenses/:id
app.get("/expenses/:id", async (req,res)=>{
    try {
        const expenseId = Number(req.params.id);
        const expense = await weddingService.findExpenseById(expenseId);
        res.status(200);
        res.send(expense);
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})
// PUT /expenses/:id
app.put("/expenses/:id", async (req,res)=>{
    try {
        let expense = req.body;
        expense = await weddingService.modifyExpenseById(expense);
        res.status(200);
        res.send(expense)
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
})
// DELETE /expenses/:id
app.delete("/expenses/:id", async (req,res)=>{
    try {
        const expenseId = Number(req.params.id);
        const isDeleted = await weddingService.removeWeddingById(expenseId);
        res.status(205);
        res.send(isDeleted);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});
app.listen(3001, ()=>{console.log("Application Started")});

