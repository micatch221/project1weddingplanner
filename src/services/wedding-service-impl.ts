import { WeddingDAO } from "../daos/wedding-dao";
import { WeddingDAODB } from "../daos/wedding-dao-impl";
import { Wedding, Expense } from "../entities";
import WeddingService from "./wedding-service";

export class WeddingServiceImpl implements WeddingService{
    
    weddingDAO:WeddingDAO = new WeddingDAODB();

    addNewWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }
    
    addNewExpense(expense: Expense): Promise<Expense> {
        return this.weddingDAO.createExpense(expense);
    }

    listAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }
    findWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(weddingId);
    }
    findExpenseById(expenseId: number): Promise<Expense> {
        return this.weddingDAO.getExpenseById(expenseId);
    }

    listAllExpensesByWeddingId(weddingId: number): Promise<Expense[]> {
        return this.weddingDAO.getAllExpensesByWeddingId(weddingId);
    }
    listAllExpenses(): Promise<Expense[]> {
        return this.weddingDAO.getAllExpenses();
    }
    modifyWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.updateWedding(wedding);
    }
    modifyExpenseById(expense:Expense): Promise<Expense> {
        return this.weddingDAO.updateExpenseById(expense);
    }
    removeWeddingById(weddingId: number): Promise<boolean> {
        return this.weddingDAO.deleteWeddingById(weddingId);
    }
    removeExpenseById(expenseId: number): Promise<boolean> {
        return this.weddingDAO.deleteExpenseById(expenseId);
    }
    
}
