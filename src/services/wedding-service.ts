import { Wedding, Expense } from "../entities";

export default interface WeddingService{

    addNewWedding(wedding:Wedding):Promise<Wedding>;
    addNewExpense(expense:Expense):Promise<Expense>;

    listAllWeddings():Promise<Wedding[]>;
    findWeddingById(weddingId:number):Promise<Wedding>;
    listAllExpensesByWeddingId(weddingId:number):Promise<Expense[]>
    listAllExpenses():Promise<Expense[]>;
    findExpenseById(expenseId:number):Promise<Expense>;

    modifyWedding(wedding:Wedding):Promise<Wedding>;
    modifyExpenseById(expense:Expense):Promise<Expense>;

    removeWeddingById(weddingId:number):Promise<boolean>;
    removeExpenseById(expenseId:number):Promise<boolean>;

}