import {client} from '../src/connection'


test("Should create a connection ", async ()=>{
    const result = await client.query('select * from wedding');
    expect(result).toBeTruthy
});

afterAll(async()=>{
    client.end()
})