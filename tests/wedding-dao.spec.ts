import { client } from "../src/connection";
import { Expense, Wedding } from "../src/entities";
import { WeddingDAO } from "../src/daos/wedding-dao";
import { WeddingDAODB } from "../src/daos/wedding-dao-impl";

const weddingDAO:WeddingDAO = new WeddingDAODB();

const testWedding:Wedding = new Wedding(0,0,"Disney World","Mickey-Minnie",55000);
const testExpense:Expense = new Expense(0,"Catering",8000,1);

test("Create new wedding", async ()=>{
    const result:Wedding = await weddingDAO.createWedding(testWedding);
    expect(result.weddingId).not.toBe(0);
});

test("Create new expense", async () =>{
    const result:Expense = await weddingDAO.createExpense(testExpense);
    expect(result.expenseId).not.toBe(0);
})

test("Get all weddings from client", async ()=>{
    let wedding1:Wedding = new Wedding(0,0,"Disney World","Mickey-Minnie",55000);
    let wedding2:Wedding = new Wedding(0,0,"Disney World","Mickey-Minnie",55000);
    await weddingDAO.createWedding(wedding1);
    await weddingDAO.createWedding(wedding2);
    const weddings:Wedding[] = await weddingDAO.getAllWeddings();
    expect (weddings[0].weddingLocation).toContain("Castle");
    //expect (weddings.length).toBeGreaterThan(1);
});

test("Get wedding by wedding id", async ()=>{
    let wedding:Wedding = new Wedding(0,0,"Disney World","Mickey-Minnie",55000);
    wedding = await weddingDAO.createWedding(wedding);
    let retrievedwedding = await weddingDAO.getWeddingById(wedding.weddingId);
    expect(retrievedwedding.weddingId).toBe(wedding.weddingId);
})

test("Get expense by expense id", async ()=>{
    let expense:Expense = new Expense(0,"Catering",8000,1);
    expense = await weddingDAO.createExpense(expense);
    let retrievedexpense = await weddingDAO.getExpenseById(expense.expenseId);
    expect(retrievedexpense.expenseId).toBe(expense.expenseId);
})

test("Get all expenses by wedding id", async ()=>{
    let expense:Expense = new Expense(0,"Catering",8000,1);
    expense = await weddingDAO.createExpense(expense);
    const expenses:Expense[] = await weddingDAO.getAllExpensesByWeddingId(expense.expWId);
    expect(expenses.length).toBeGreaterThan(1);
})

test("Get all expenses", async () =>{
    const expenses:Expense[] = await weddingDAO.getAllExpenses();
    expect(expenses.length).toBeGreaterThan(1);
})


test("update wedding", async () => {
    let wedding:Wedding = new Wedding(0,0,"Test Land","Update-test",55000);
    wedding = await weddingDAO.createWedding(wedding);
    wedding.weddingBudget = 444;
    await weddingDAO.updateWedding(wedding);
    const updatedwedding = await weddingDAO.getWeddingById(wedding.weddingId);
    expect(updatedwedding.weddingBudget).toBe(444);
})

test("update expense", async () => {
    let expense:Expense = new Expense(0,"Testing",8000,1);
    expense = await weddingDAO.createExpense(expense);
    expense.expenseAmount = 444;
    await weddingDAO.updateExpenseById(expense);
    const updatedExpense = await weddingDAO.getExpenseById(expense.expenseId);
    expect(updatedExpense.expenseAmount).toBe(444);
})

test("delete wedding by wedding id", async ()=>{
    let wedding:Wedding = new Wedding(0,0,'delete test','delete',2);
    wedding = await weddingDAO.createWedding(wedding);
    const result:boolean = await weddingDAO.deleteWeddingById(wedding.weddingId);
    expect(result).toBeTruthy()
})

test("delete wedding by wedding id", async ()=>{
    let wedding:Wedding = new Wedding(0,0,'delete test','delete',2);
    wedding = await weddingDAO.createWedding(wedding);
    const result:boolean = await weddingDAO.deleteWeddingById(wedding.weddingId);
    expect(result).toBeTruthy()
})

test("delete expense by expense id", async ()=>{
    let expense:Expense = new Expense(0,"Delete Test",8000,1);
    expense = await weddingDAO.createExpense(expense);
    const result:boolean = await weddingDAO.deleteExpenseById(expense.expenseId);
    expect(result).toBeTruthy()
})
afterAll(async()=>{
    client.end()
})